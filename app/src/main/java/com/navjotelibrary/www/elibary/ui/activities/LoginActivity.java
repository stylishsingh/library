package com.navjotelibrary.www.elibary.ui.activities;

import android.os.Bundle;

import com.navjotelibrary.www.elibary.R;
import com.navjotelibrary.www.elibary.base.BaseActivity;
import com.navjotelibrary.www.elibary.ui.fragments.common.LoginFragment;

public class LoginActivity extends BaseActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        navigateToWithBundle(R.id.container,new LoginFragment(),LoginFragment.class.getSimpleName(),true,null);
    }
}
