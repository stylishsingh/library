package com.navjotelibrary.www.elibary.ui.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.navjotelibrary.www.elibary.R;
import com.navjotelibrary.www.elibary.base.BaseActivity;
import com.navjotelibrary.www.elibary.ui.fragments.admin.AddBook;
import com.navjotelibrary.www.elibary.ui.fragments.admin.EditBook;
import com.navjotelibrary.www.elibary.ui.fragments.user.SignOutBookDetail;
import com.navjotelibrary.www.elibary.ui.fragments.user.ReadBookFragment;
import com.navjotelibrary.www.elibary.ui.fragments.user.ViewBook;

import static com.navjotelibrary.www.elibary.utils.CurrentScreen.ADD_BOOK;
import static com.navjotelibrary.www.elibary.utils.CurrentScreen.CURRENT_SCREEN;
import static com.navjotelibrary.www.elibary.utils.CurrentScreen.DETAIL_BOOK;
import static com.navjotelibrary.www.elibary.utils.CurrentScreen.EDIT_BOOK;
import static com.navjotelibrary.www.elibary.utils.CurrentScreen.READ_BOOK;
import static com.navjotelibrary.www.elibary.utils.CurrentScreen.SIGN_OUT_DETAIL_BOOK;


public class HolderActivity extends BaseActivity {

    public Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_holder);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("");
        }

        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        if (getIntent() != null) {
            String screen = getIntent().getStringExtra(CURRENT_SCREEN);
            switch (screen) {
                case ADD_BOOK:
                    toolbar.setTitle("Add Book");
                    navigateToWithBundle(R.id.container, new AddBook(), AddBook.class.getSimpleName(), false, null);
                    break;

                case EDIT_BOOK:
                    toolbar.setTitle("Edit Book");
                    navigateToWithBundle(R.id.container, new EditBook(), EditBook.class.getSimpleName(), false, getIntent().getExtras());
                    break;

                case DETAIL_BOOK:
                    toolbar.setTitle("Book Details");
                    navigateToWithBundle(R.id.container, new ViewBook(), ViewBook.class.getSimpleName(), false, getIntent().getExtras());
                    break;

                case SIGN_OUT_DETAIL_BOOK:
                    toolbar.setTitle("Book Details");
                    navigateToWithBundle(R.id.container, new SignOutBookDetail(), SignOutBookDetail.class.getSimpleName(), false, getIntent().getExtras());
                    break;

            }
        }
    }

    @Override
    public void onBackPressed() {

        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
        if (fragment instanceof ReadBookFragment) {
            getSupportFragmentManager().popBackStack();
            toolbar.setTitle("Book Detail");
        } else {
            finish();
        }

    }
}
