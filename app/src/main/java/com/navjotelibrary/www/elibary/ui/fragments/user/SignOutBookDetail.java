package com.navjotelibrary.www.elibary.ui.fragments.user;


import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.navjotelibrary.www.elibary.DatabaseHandler;
import com.navjotelibrary.www.elibary.R;
import com.navjotelibrary.www.elibary.ui.activities.HolderActivity;
import com.navjotelibrary.www.elibary.ui.models.Item;
import com.navjotelibrary.www.elibary.utils.CurrentScreen;

/**
 * A simple {@link Fragment} subclass.
 */
public class SignOutBookDetail extends Fragment {

    TextView txt_name;
    ImageView cover_book;
    Button return_book, read_book;

    DatabaseHandler db;
    private String linktext;
    private String itemid;

    public SignOutBookDetail() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_sign_out_book_detail, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        db = new DatabaseHandler(getContext());
        cover_book = view.findViewById(R.id.cover_book);
        txt_name = view.findViewById(R.id.txt_name);
        read_book = view.findViewById(R.id.read_book);
        return_book = view.findViewById(R.id.return_book);


        if (getArguments() != null) {

            String id = getArguments().getString("id");
            final int itemId = Integer.parseInt(id);

            Item item = db.getItem(itemId);
            String drawableName = item.getCover();
            txt_name.setText(item.getName());
            int resID = getResources().getIdentifier(drawableName, "drawable", getContext().getPackageName());
            cover_book.setImageResource(resID);
            linktext = item.getURLLINK();
            itemid = String.valueOf(itemId);
        }


        read_book.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (getActivity() != null) {
                    Bundle bundle = new Bundle();
                    bundle.putString("url", linktext);
                    ((HolderActivity) getActivity()).toolbar.setTitle(txt_name.getText().toString());
                    ((HolderActivity) getActivity()).navigateToWithBundle(R.id.container,
                            new ReadBookFragment(), ReadBookFragment.class.getSimpleName(),
                            true, bundle);
                }
            }
        });


        return_book.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertMessage(itemid);
            }
        });
    }

    public void alertMessage(final String itemid) {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (getActivity() != null) {
                    switch (which) {
                        case DialogInterface.BUTTON_POSITIVE:
                            //Do your Yes progress
                            db.returnItem(itemid);
                            Toast.makeText(getContext(), "Book Returned", Toast.LENGTH_SHORT).show();
                            getActivity().sendBroadcast(new Intent(CurrentScreen.DETAIL_BOOK));
                            getActivity().onBackPressed();
                            break;

                        case DialogInterface.BUTTON_NEGATIVE:
                            //Do your No progress
                            break;
                    }
                }
            }
        };

        if (getContext() != null) {
            AlertDialog.Builder ab = new AlertDialog.Builder(getContext(), R.style.AlertDialogCustom);
            ab.setTitle("Are you sure to Return?").setPositiveButton("Yes", dialogClickListener)
                    .setNegativeButton("No", dialogClickListener).show();
        }
    }
}
