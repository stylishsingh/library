package com.navjotelibrary.www.elibary.ui.models;

public class Item {
    private int item_id;
    private String name;
    private String price;
    private String isbn;
    private String category;
    private String stock;
    private String cover;
    private String urllink;

    public Item() {
    }

    public Item(String name, String price, String isbn, String category,String stock,String cover, String urllink) {
        this.name = name;
        this.price = price;
        this.isbn = isbn;
        this.category = category;
        this.stock = stock;
        this.cover = cover;
        this.urllink = urllink;
    }

    public int getItem_id() {
        return item_id;
    }

    public void setItem_id(int item_id) {
        this.item_id = item_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getStock() {
        return stock;
    }

    public void setStock(String stock) {
        this.stock = stock;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public String getURLLINK() {
        return urllink;
    }

    public void setURLLINK(String link) {
        this.urllink = link;
    }
}
