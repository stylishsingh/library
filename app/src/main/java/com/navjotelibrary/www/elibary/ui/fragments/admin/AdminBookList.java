package com.navjotelibrary.www.elibary.ui.fragments.admin;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.navjotelibrary.www.elibary.DatabaseHandler;
import com.navjotelibrary.www.elibary.R;
import com.navjotelibrary.www.elibary.ui.activities.HolderActivity;
import com.navjotelibrary.www.elibary.ui.adapters.AdminUserBookAdapter;
import com.navjotelibrary.www.elibary.ui.models.AdminBookModel;
import com.navjotelibrary.www.elibary.utils.CurrentScreen;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class AdminBookList extends Fragment {

    RecyclerView recyclerView;
    LinearLayoutManager linearLayoutManager;
    AdminUserBookAdapter adapter;
    private List<AdminBookModel> list;
    private DatabaseHandler db;

    public AdminBookList() {
        // Required empty public constructor
    }

    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            updateList();
        }
    };

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_admin_book_list, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getActivity() != null)
            getActivity().registerReceiver(broadcastReceiver, new IntentFilter(CurrentScreen.RECEIVER_BOOK));
        recyclerView = view.findViewById(R.id.recycler_view);
        linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);


        FloatingActionButton fab = view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(new Intent(getActivity(), HolderActivity.class)
                        .putExtra(CurrentScreen.CURRENT_SCREEN, CurrentScreen.ADD_BOOK), 100);
            }
        });


        updateList();

    }

    public void updateList() {
        db = new DatabaseHandler(getContext());
        Cursor cursor = db.fetchAllItems();

        list = new ArrayList<>();
        if (cursor.getCount() > 0) {
            do {
                AdminBookModel model = new AdminBookModel();
                model.setId(cursor.getString(0));
                model.setTitle(cursor.getString(1));
                list.add(model);
            } while (cursor.moveToNext());
        }

        adapter = new AdminUserBookAdapter(this, getContext(), list, db, true);
        recyclerView.setAdapter(adapter);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (getActivity() != null)
            getActivity().unregisterReceiver(broadcastReceiver);
    }
}
