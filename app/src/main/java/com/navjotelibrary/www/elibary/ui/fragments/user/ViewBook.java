package com.navjotelibrary.www.elibary.ui.fragments.user;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.navjotelibrary.www.elibary.DatabaseHandler;
import com.navjotelibrary.www.elibary.ui.models.Item;
import com.navjotelibrary.www.elibary.R;
import com.navjotelibrary.www.elibary.ui.models.Purchase;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class ViewBook extends Fragment {

    ImageView cover_book;
    TextView txt_name, txt_category, txt_price, txt_total, txt_stock;
    Button getBook;

    DatabaseHandler db;

    public ViewBook() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_view_book, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        db = new DatabaseHandler(getContext());
        cover_book = view.findViewById(R.id.cover_book);
        txt_name = view.findViewById(R.id.txt_name);
        txt_category = view.findViewById(R.id.txt_category);
        txt_price = view.findViewById(R.id.txt_price);
        txt_total = view.findViewById(R.id.txt_total);
        txt_stock = view.findViewById(R.id.txt_stock);
        getBook = view.findViewById(R.id.getBook);

        if (getArguments() != null) {


            String id = getArguments().getString("id");
            final int itemId = Integer.parseInt(id);

            Item item = db.getItem(itemId);
            String drawableName = item.getCover();
            int resID = getResources().getIdentifier(drawableName, "drawable", getContext().getPackageName());
            cover_book.setImageResource(resID);


            txt_name.setText("Name: " + item.getName());
            txt_category.setText("Category: " + item.getCategory());
            txt_price.setText("Price: $" + item.getPrice());
            txt_total.setText("Total: $11");
            txt_stock.setText("Stock: " + item.getStock());


            int stock = Integer.parseInt(item.getStock());
            if (stock < 1) {
                txt_stock.setText("Stock: Unavailable");
                getBook.setVisibility(View.INVISIBLE);
            }

            final String itemid = String.valueOf(itemId);

            getBook.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (getActivity()!=null) {
                        String stock_ = db.getBook(itemid);

                        List<Purchase> purchase = db.getAllPurchases();
                        String result = "";
                        for (Purchase cn : purchase) {
                            result += "ID: " + cn.getId() + " - ItemID: " + cn.getItem_id() + " \n";
                            //String log = "Id: " + cn.getID() + " ,Name: " + cn.getName() + " ,Phone: " + cn.getPhoneNumber();
                            // Writing Contacts to log

                        }
                        Log.e("Purchases: ", result);

                        Toast.makeText(getContext(), "Book Added to Reading List", Toast.LENGTH_SHORT).show();

                        getActivity().onBackPressed();
                    }

                }
            });
        }
    }
}
