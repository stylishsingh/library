package com.navjotelibrary.www.elibary.base;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.navjotelibrary.www.elibary.R;

public abstract class BaseActivity extends AppCompatActivity {

    protected ProgressDialog mProgressDialogOrange;

    public void navigateToWithBundle(int container, Fragment fragment, String extraTag, boolean isBackStack, Bundle bundle) {
        String finalTag = fragment.getClass().getSimpleName();
        fragment.setArguments(bundle);
        FragmentTransaction fts = getSupportFragmentManager().beginTransaction();
        fts.replace(container, fragment, finalTag);
        if (isBackStack)
            fts.addToBackStack(finalTag);
        fts.commit();
    }


    public void clearBackStack() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        //this will clear the back stack and displays no animation on the screen
        int backStackCount = fragmentManager.getBackStackEntryCount();
        for (int i = 0; i < backStackCount; i++) {
            // Get the back stack fragment id.
            int backStackId = fragmentManager.getBackStackEntryAt(i).getId();
            fragmentManager.popBackStack(backStackId, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    }

    public void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
            getSupportFragmentManager().popBackStack();
        } else {
            //super.onBackPressed();
            finish();
        }
    }

    public void showProgressDialogOrange() {
        if (mProgressDialogOrange == null) {
            mProgressDialogOrange = ProgressDialog.show(this, null, null, true);
            mProgressDialogOrange.setContentView(R.layout.progress_bar);
            mProgressDialogOrange.setCancelable(false);
            if (mProgressDialogOrange.getWindow() != null)
                mProgressDialogOrange.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        } else mProgressDialogOrange.show();
    }


    public void hideProgressDialogOrange() {
        if (mProgressDialogOrange != null && mProgressDialogOrange.isShowing()) {
            mProgressDialogOrange.dismiss();
        }
    }


    public boolean isShowingOrange() {
        return mProgressDialogOrange != null && mProgressDialogOrange.isShowing();
    }
}
