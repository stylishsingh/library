package com.navjotelibrary.www.elibary.ui.fragments.admin;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.navjotelibrary.www.elibary.DatabaseHandler;
import com.navjotelibrary.www.elibary.R;
import com.navjotelibrary.www.elibary.ui.models.Item;
import com.navjotelibrary.www.elibary.utils.CurrentScreen;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddBook extends Fragment {

    Button btn_add_item;
    EditText txt_itemname, txt_itemprice, txt_isbn, txt_category, txt_url;

    DatabaseHandler db;

    public AddBook() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_add_book, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        db = new DatabaseHandler(getContext());

        btn_add_item = view.findViewById(R.id.btn_add_item);

        txt_itemname = view.findViewById(R.id.txt_itemname);
        txt_itemprice = view.findViewById(R.id.txt_itemprice);
        txt_isbn = view.findViewById(R.id.txt_isbn);
        txt_category = view.findViewById(R.id.txt_category);
        txt_url = view.findViewById(R.id.txt_url);

        btn_add_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (getActivity() != null) {
                    if (txt_itemname.getText().toString().length() == 0 || txt_itemprice.getText().toString().length() == 0 || txt_isbn.getText().toString().length() == 0 || txt_category.getText().toString().length() == 0) {

                        //Show required error
                        Toast.makeText(getContext(), "All Fields are Required", Toast.LENGTH_SHORT).show();
                    } else {
                        //store the user
                        db.addItem(new Item(txt_itemname.getText().toString(), txt_itemprice.getText().toString(), txt_isbn.getText().toString(), txt_category.getText().toString(), "5", "agenda", txt_url.getText().toString()));

                        txt_itemname.setText("");
                        txt_itemprice.setText("");
                        txt_isbn.setText("");
                        txt_category.setText("");
                        txt_url.setText("");

                        getActivity().sendBroadcast(new Intent(CurrentScreen.RECEIVER_BOOK));
                        getActivity().onBackPressed();

                        Toast.makeText(getContext(), "New Item Added", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

    }
}
