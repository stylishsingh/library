package com.navjotelibrary.www.elibary.ui.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.navjotelibrary.www.elibary.DatabaseHandler;
import com.navjotelibrary.www.elibary.R;
import com.navjotelibrary.www.elibary.ui.activities.HolderActivity;
import com.navjotelibrary.www.elibary.ui.fragments.admin.AdminBookList;
import com.navjotelibrary.www.elibary.ui.fragments.user.SignOutBook;
import com.navjotelibrary.www.elibary.ui.fragments.user.UserBookList;
import com.navjotelibrary.www.elibary.ui.models.AdminBookModel;
import com.navjotelibrary.www.elibary.utils.CurrentScreen;

import java.util.List;

import static com.navjotelibrary.www.elibary.utils.CurrentScreen.DETAIL_BOOK;
import static com.navjotelibrary.www.elibary.utils.CurrentScreen.EDIT_BOOK;
import static com.navjotelibrary.www.elibary.utils.CurrentScreen.SIGN_OUT_DETAIL_BOOK;

public class AdminUserBookAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final List<AdminBookModel> list;
    private final Context context;
    private final DatabaseHandler db;
    private final Fragment fragment;
    private final boolean isDelete;

    public AdminUserBookAdapter(Fragment fragment, Context context, List<AdminBookModel> list, DatabaseHandler db, boolean isDelete) {
        this.list = list;
        this.context = context;
        this.db = db;
        this.fragment = fragment;
        this.isDelete = isDelete;
    }

    public AdminUserBookAdapter(UserBookList fragment, Context context, List<AdminBookModel> list, DatabaseHandler db, boolean isDelete) {
        this.list = list;
        this.context = context;
        this.db = db;
        this.fragment = fragment;
        this.isDelete = isDelete;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_books, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolder) {
            ViewHolder viewHolder = (ViewHolder) holder;
            viewHolder.tvBookTitle.setText(list.get(viewHolder.getAdapterPosition()).getTitle());
            viewHolder.cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent = new Intent(context, HolderActivity.class);
                    intent.putExtra("id", list.get(holder.getAdapterPosition()).getId());
                    if (isDelete) {
                        intent.putExtra(CurrentScreen.CURRENT_SCREEN, EDIT_BOOK);
                    } else {
                        if (fragment instanceof UserBookList)
                            intent.putExtra(CurrentScreen.CURRENT_SCREEN, DETAIL_BOOK);
                        else if (fragment instanceof SignOutBook)
                            intent.putExtra(CurrentScreen.CURRENT_SCREEN, SIGN_OUT_DETAIL_BOOK);
                    }
                    context.startActivity(intent);
                }
            });

            viewHolder.ivDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertMessage(Integer.parseInt(list.get(holder.getAdapterPosition()).getId()));
                }
            });

        }
    }

    public void alertMessage(final int itemId) {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        //Do your Yes progress
                        db.deleteItem(itemId);
                        if (fragment instanceof AdminBookList) {
                            AdminBookList adminBookList = (AdminBookList) fragment;
                            adminBookList.updateList();
                        }
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        //Do your No progress
                        break;
                }
            }
        };

        AlertDialog.Builder ab = new AlertDialog.Builder(context, R.style.AlertDialogCustom);
        ab.setTitle("Are you sure to delete?").setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvBookTitle;
        CardView cardView;
        ImageView ivDelete;

        public ViewHolder(View itemView) {
            super(itemView);
            tvBookTitle = itemView.findViewById(R.id.item_name);
            cardView = itemView.findViewById(R.id.cv);
            ivDelete = itemView.findViewById(R.id.iv_delete);

            if (isDelete) {
                ivDelete.setVisibility(View.VISIBLE);
            } else {
                ivDelete.setVisibility(View.GONE);
            }
        }
    }
}
