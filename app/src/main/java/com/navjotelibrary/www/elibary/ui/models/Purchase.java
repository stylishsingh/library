package com.navjotelibrary.www.elibary.ui.models;

public class Purchase {

    int id;
    int item_id;

    public Purchase(int id, int item_id) {
        this.id = id;
        this.item_id = item_id;
    }

    public Purchase() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getItem_id() {
        return item_id;
    }

    public void setItem_id(int item_id) {
        this.item_id = item_id;
    }
}
