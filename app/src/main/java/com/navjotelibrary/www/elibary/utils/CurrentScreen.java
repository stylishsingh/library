package com.navjotelibrary.www.elibary.utils;

public interface CurrentScreen {

    String READ_BOOK ="READ_BOOK";
    String EDIT_BOOK ="EDIT_BOOK";
    String DETAIL_BOOK ="DETAIL_BOOK";
    String SIGN_OUT_DETAIL_BOOK ="SIGN_OUT_DETAIL_BOOKt";
    String CURRENT_SCREEN = "CURRENT_SCREEN ";
    String ADD_BOOK = "ADD_BOOK";
    String RECEIVER_BOOK="RECEIVER_BOOK";
}
