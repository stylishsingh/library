package com.navjotelibrary.www.elibary.ui.fragments.common;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.navjotelibrary.www.elibary.DatabaseHandler;
import com.navjotelibrary.www.elibary.R;
import com.navjotelibrary.www.elibary.ui.activities.DashboardAdmin;
import com.navjotelibrary.www.elibary.ui.activities.DashboardUser;
import com.navjotelibrary.www.elibary.ui.activities.LoginActivity;

public class LoginFragment extends Fragment {

    TextView txt_forgot;
    TextView txt_usr_email;
    TextView txt_usr_pwd, txtRegister;
    Button btn_login;
    DatabaseHandler db;


    public LoginFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_login, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        db = new DatabaseHandler(getContext());
        txt_usr_email = (EditText) view.findViewById(R.id.txt_email);
        txt_usr_pwd = (EditText) view.findViewById(R.id.txt_usr_pwd);
        btn_login = view.findViewById(R.id.btn_login);
        txt_forgot = view.findViewById(R.id.txt_forgot);
        txtRegister = view.findViewById(R.id.txt_register);

        SpannableString forgotString = new SpannableString(txt_forgot.getText().toString().trim());
        forgotString.setSpan(new UnderlineSpan(), 0, forgotString.length(), 0);
        txt_forgot.setText(forgotString);

        SpannableString registerString = new SpannableString(txtRegister.getText().toString().trim());
        registerString.setSpan(new UnderlineSpan(), 0, registerString.length(), 0);
        txtRegister.setText(registerString);

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getActivity() != null) {
                    String user_ = db.loginUser(txt_usr_email.getText().toString(), txt_usr_pwd.getText().toString());
                    String s = user_ + " Role";
                    Log.e("user: ", s);
                    db.close();

                    if (user_.equals("A")) {
                        Intent i = new Intent(getActivity(), DashboardAdmin.class);
                        startActivity(i);
                        getActivity().finish();
                    } else if (user_.equals("U")) {
                        Intent i = new Intent(getActivity(), DashboardUser.class);
                        startActivity(i);
                        getActivity().finish();
                    } else {
                        Toast.makeText(getActivity(), "Wrong Email/Pwd", Toast.LENGTH_SHORT).show();
                    }
                }

            }
        });

        txt_forgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(getApplicationContext(),"Hell there",Toast.LENGTH_LONG).show();
                if (getActivity() != null)
                    ((LoginActivity) getActivity()).navigateToWithBundle(R.id.container, new ForgotPassword(), ForgotPassword.class.getSimpleName(), true, null);
            }
        });

        txtRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(getApplicationContext(),"Hell there",Toast.LENGTH_LONG).show();
                if (getActivity() != null)
                    ((LoginActivity) getActivity()).navigateToWithBundle(R.id.container, new SignUpFragment(), SignUpFragment.class.getSimpleName(), true, null);
            }
        });


    }
}