package com.navjotelibrary.www.elibary.ui.models;

public class AdminBookModel {
    private String title;
    private String id;


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
