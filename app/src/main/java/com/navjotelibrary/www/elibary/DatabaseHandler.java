package com.navjotelibrary.www.elibary;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.navjotelibrary.www.elibary.ui.models.Item;
import com.navjotelibrary.www.elibary.ui.models.Purchase;
import com.navjotelibrary.www.elibary.ui.models.User;

import java.util.ArrayList;
import java.util.List;

public class DatabaseHandler extends SQLiteOpenHelper {

    //Static Variables
    private static final int DATABASE_VERSION = 13;
    private static final String DATABASE_NAME = "eLibraryManager";

    private static final String TABLE_USERS = "users";
    //Column Names
    private static final String KEY_ID = "user_id";
    private static final String KEY_FNAME = "fname";
    private static final String KEY_LNAME = "lname";
    private static final String KEY_EMAIL = "email";
    private static final String KEY_PWD = "pwd";
    private static final String KEY_ROLE = "role";


    private static final String TABLE_ITEMS = "items";
    //Column Names
    private static final String KEY_ITEM_ID = "item_id";
    private static final String KEY_ITEM_NAME = "name";
    private static final String KEY_ITEM_PRICE = "price";
    private static final String KEY_ITEM_ISBN = "isbn";
    private static final String KEY_ITEM_CATEGORY = "category";
    private static final String KEY_ITEM_STOCK = "stock";
    private static final String KEY_ITEM_COVER = "cover";
    private static final String KEY_ITEM_URLLINK = "urllink";


    private static final String TABLE_PURCHASE = "purchase";

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_USERS_TABLE = "CREATE TABLE " + TABLE_USERS + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_FNAME + " TEXT,"
                + KEY_LNAME + " TEXT," + KEY_EMAIL + " TEXT, " + KEY_PWD + " TEXT, " + KEY_ROLE + " TEXT )";
        db.execSQL(CREATE_USERS_TABLE);

        String INSERT_USERS_TABLE = "insert into " + TABLE_USERS + " (user_id,fname,lname,email,pwd,role) values (1,'Admin','Admin','admin@gmail.com','1234','A')";
        db.execSQL(INSERT_USERS_TABLE);
        String INSERT_USERS_TABLE2 = "insert into " + TABLE_USERS + " (user_id,fname,lname,email,pwd,role) values (2,'Navjot','Kaur','navjot@gmail.com','1234','U')";
        db.execSQL(INSERT_USERS_TABLE2);


        String CREATE_ITEMS_TABLE = "CREATE TABLE " + TABLE_ITEMS + "("
                + KEY_ITEM_ID + " INTEGER PRIMARY KEY," + KEY_ITEM_NAME + " TEXT,"
                + KEY_ITEM_PRICE + " TEXT," + KEY_ITEM_ISBN + " TEXT, " + KEY_ITEM_CATEGORY + " TEXT, " + KEY_ITEM_STOCK + " TEXT, " + KEY_ITEM_COVER + " TEXT, " + KEY_ITEM_URLLINK + " TEXT )";
        db.execSQL(CREATE_ITEMS_TABLE);

        String INSERT_BOOK1 = "insert into " + TABLE_ITEMS + " (item_id,name,price,isbn,category,stock,cover,urllink) values (1,'Emma by Jane Austen','10.99','523DWEX21','Non Fiction','5','@drawable/book1','https://www.gutenberg.org/files/158/158-h/158-h.htm')";
        db.execSQL(INSERT_BOOK1);

        String INSERT_BOOK2 = "insert into " + TABLE_ITEMS + " (item_id,name,price,isbn,category,stock,cover,urllink) values (2,'Ulysses by James Joyce','10.99','42XWGVQ3','Fiction','5','@drawable/book2','http://www.gutenberg.org/files/4300/4300-h/4300-h.htm')";
        db.execSQL(INSERT_BOOK2);

        String INSERT_BOOK3 = "insert into " + TABLE_ITEMS + " (item_id,name,price,isbn,category,stock,cover,urllink) values (3,'Great Expectations by Charles Dickens','10.99','RRDWEB232','Fiction','5','@drawable/book3','https://www.gutenberg.org/files/1400/1400-h/1400-h.htm')";
        db.execSQL(INSERT_BOOK3);

        String INSERT_BOOK4 = "insert into " + TABLE_ITEMS + " (item_id,name,price,isbn,category,stock,cover,urllink) values (4,'Les Misérables by Victor Hugo','11.99','V23756BHJ','Fiction','5','@drawable/book4','https://www.gutenberg.org/files/135/135-h/135-h.htm')";
        db.execSQL(INSERT_BOOK4);

        String INSERT_BOOK5 = "insert into " + TABLE_ITEMS + " (item_id,name,price,isbn,category,stock,cover,urllink) values (5,'Leviathan by Thomas Hobbes','7.99','MNJISDe','Non Fiction','5','@drawable/book5','https://www.gutenberg.org/files/3207/3207-h/3207-h.htm')";
        db.execSQL(INSERT_BOOK5);

        String CREATE_PURCHAE_TABLE = "CREATE TABLE " + TABLE_PURCHASE + "("
                + KEY_ID + " INTEGER," + KEY_ITEM_ID + " TEXT)";
        db.execSQL(CREATE_PURCHAE_TABLE);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_USERS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ITEMS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PURCHASE);

        // Create tables again
        onCreate(db);
    }


    // Adding new Users
    public void addContact(User user) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_FNAME, user.getFname());
        values.put(KEY_LNAME, user.getLname());
        values.put(KEY_EMAIL, user.getEmail());
        values.put(KEY_PWD, user.getPwd());
        values.put(KEY_ROLE, "U");

        // Inserting Row
        db.insert(TABLE_USERS, null, values);
        db.close(); // Closing database connection
    }


    // Getting All Users
    public List<User> getAllUsers() {
        List<User> userList = new ArrayList<User>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_USERS;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                User user = new User();
                user.setId(Integer.parseInt(cursor.getString(0)));
                user.setFname(cursor.getString(1));
                user.setLname(cursor.getString(2));
                user.setEmail(cursor.getString(3));
                user.setPwd(cursor.getString(4));
                // Adding contact to list
                userList.add(user);
            } while (cursor.moveToNext());
        }

        // return contact list
        return userList;
    }


    public void addItem(Item item) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_ITEM_NAME, item.getName());
        values.put(KEY_ITEM_PRICE, item.getPrice());
        values.put(KEY_ITEM_ISBN, item.getIsbn());
        values.put(KEY_ITEM_CATEGORY, item.getCategory());
        values.put(KEY_ITEM_STOCK, item.getStock());
        values.put(KEY_ITEM_COVER, item.getCover());
        values.put(KEY_ITEM_URLLINK, item.getURLLINK());

        // Inserting Row
        db.insert(TABLE_ITEMS, null, values);
        db.close(); // Closing database connection
    }

    public Cursor fetchAllItems() {

        String selectQuery = "SELECT  item_id as _id,name FROM " + TABLE_ITEMS;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor mCursor = db.rawQuery(selectQuery, null);


        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;
    }

    public Cursor fetchMyItems() {

        String selectQuery = "SELECT  i.item_id as _id,name FROM " + TABLE_ITEMS + " as i, " + TABLE_PURCHASE + " as p where i.item_id=p.item_id and user_id=2";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor mCursor = db.rawQuery(selectQuery, null);


        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;
    }

    public Item getItem(int id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_ITEMS, new String[]{KEY_ITEM_ID,
                        KEY_ITEM_NAME, KEY_ITEM_PRICE, KEY_ITEM_ISBN, KEY_ITEM_CATEGORY, KEY_ITEM_STOCK, KEY_ITEM_COVER, KEY_ITEM_URLLINK}, KEY_ITEM_ID + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        Item item = new Item(
                cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4), cursor.getString(5), cursor.getString(6), cursor.getString(7));
        // return contact
        return item;
    }

    public int updateItem(int itemId, String itemname, String itemprice, String isbn, String category) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_ITEM_NAME, itemname);
        values.put(KEY_ITEM_PRICE, itemprice);
        values.put(KEY_ITEM_ISBN, isbn);
        values.put(KEY_ITEM_CATEGORY, category);

        // updating row
        return db.update(TABLE_ITEMS, values, KEY_ITEM_ID + " = ?",
                new String[]{String.valueOf(itemId)});
    }

    public void deleteItem(int item) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_ITEMS, KEY_ITEM_ID + " = ?",
                new String[]{String.valueOf(item)});
        db.close();
    }

    public String loginUser(String email, String pwd) {

        String[] columns = {
                KEY_ID,
                KEY_ROLE
        };
        SQLiteDatabase db = this.getReadableDatabase();
        String selection = KEY_EMAIL + " = ? " + " AND " + KEY_PWD + " = ? ";

        String[] selectionArgs = {email, pwd};

        Cursor cursor = db.query(TABLE_USERS,
                columns,
                selection,
                selectionArgs,
                null,
                null,
                null);

        int cursorCount = cursor.getCount();

        String s = "";
        if (cursorCount > 0) {
            if (cursor.moveToFirst()) {
                s = cursor.getString(cursor.getColumnIndex(KEY_ROLE));
            }
        }

        cursor.close();
        db.close();
        if (cursorCount > 0) {
            return s;
        }

        return "0";
    }


    public boolean verifyEmail(String email) {

        String[] columns = {
                KEY_ID,
                KEY_ROLE
        };
        SQLiteDatabase db = this.getReadableDatabase();
        String selection = KEY_EMAIL + " = ? ";

        String[] selectionArgs = {email};

        Cursor cursor = db.query(TABLE_USERS,
                columns,
                selection,
                selectionArgs,
                null,
                null,
                null);

        int cursorCount = cursor.getCount();

        cursor.close();
        db.close();
        return cursorCount > 0;

    }

    public int updatePassword(String email, String password) {

        String[] columns = {
                KEY_ID,
                KEY_ROLE
        };
        SQLiteDatabase db = this.getReadableDatabase();
        String selection = KEY_EMAIL + " = ? ";

        String[] selectionArgs = {email};

        Cursor cursor = db.query(TABLE_USERS,
                columns,
                selection,
                selectionArgs,
                null,
                null,
                null);

        //Updating Stock
        ContentValues values = new ContentValues();
        values.put(KEY_PWD, password);
        int count = db.update(TABLE_USERS, values, KEY_EMAIL + " = ?",
                new String[]{String.valueOf(email)});

        cursor.close();
        db.close();
        return count;

    }


    public String getBook(String item_id) {
        String[] columns = {
                KEY_ITEM_ID,
                KEY_ITEM_STOCK
        };
        SQLiteDatabase db = this.getReadableDatabase();
        String selection = KEY_ITEM_ID + " = ? ";

        String[] selectionArgs = {item_id};

        Cursor cursor = db.query(TABLE_ITEMS,
                columns,
                selection,
                selectionArgs,
                null,
                null,
                null);

        int cursorCount = cursor.getCount();
        //Getting Stock
        String s = "";
        if (cursorCount > 0) {
            if (cursor.moveToFirst()) {
                s = cursor.getString(cursor.getColumnIndex(KEY_ITEM_STOCK));
            }
        }

        //Calculating New Stock
        int stock = Integer.parseInt(s);
        stock = stock - 1;
        String st = String.valueOf(stock);

        //Updating Stock
        ContentValues values = new ContentValues();
        values.put(KEY_ITEM_STOCK, st);
        db.update(TABLE_ITEMS, values, KEY_ITEM_ID + " = ?",
                new String[]{String.valueOf(item_id)});

        //Adding Purchase Entry
        ContentValues values_ = new ContentValues();
        values_.put(KEY_ID, 2);
        values_.put(KEY_ITEM_ID, item_id);
        db.insert(TABLE_PURCHASE, null, values_);

        cursor.close();
        db.close();

        return st;
    }

    //Returning Book
    public String returnItem(String item_id) {
        String[] columns = {
                KEY_ITEM_ID,
                KEY_ITEM_STOCK
        };
        SQLiteDatabase db = this.getReadableDatabase();
        String selection = KEY_ITEM_ID + " = ? ";

        String[] selectionArgs = {item_id};

        Cursor cursor = db.query(TABLE_ITEMS,
                columns,
                selection,
                selectionArgs,
                null,
                null,
                null);

        int cursorCount = cursor.getCount();
        //Getting Stock
        String s = "";
        if (cursorCount > 0) {
            if (cursor.moveToFirst()) {
                s = cursor.getString(cursor.getColumnIndex(KEY_ITEM_STOCK));
            }
        }

        //Calculating New Stock
        int stock = Integer.parseInt(s);
        stock = stock + 1;
        String st = String.valueOf(stock);

        //Updating Stock
        ContentValues values = new ContentValues();
        values.put(KEY_ITEM_STOCK, st);
        db.update(TABLE_ITEMS, values, KEY_ITEM_ID + " = ?",
                new String[]{String.valueOf(item_id)});

        //Deleting Purchase Entry
        ContentValues values_ = new ContentValues();
        //db.delete(TABLE_PURCHASE, KEY_ITEM_ID + " = ?",
        //        new String[] { String.valueOf(item_id) },null,null,null);

        db.execSQL("delete from purchase where item_id in (select distinct item_id from purchase where item_id='" + item_id + "' limit 1)");

        cursor.close();
        db.close();

        return st;
    }

    public List<Purchase> getAllPurchases() {
        List<Purchase> userList = new ArrayList<Purchase>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_PURCHASE;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Purchase user = new Purchase();
                user.setId(Integer.parseInt(cursor.getString(0)));
                user.setItem_id(Integer.parseInt(cursor.getString(1)));
                // Adding contact to list
                userList.add(user);
            } while (cursor.moveToNext());
        }

        // return contact list
        return userList;
    }

}
