package com.navjotelibrary.www.elibary.ui.fragments.common;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.navjotelibrary.www.elibary.DatabaseHandler;
import com.navjotelibrary.www.elibary.R;
import com.navjotelibrary.www.elibary.ui.models.User;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class SignUpFragment extends Fragment {

    Button btn_reg;
    EditText txt_fname, txt_lname, txt_email, txt_pwd1, txt_pwd2;

    //Creating Object from DatabaseHandler
    DatabaseHandler db;

    public SignUpFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_sign_up, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        db = new DatabaseHandler(getContext());
        btn_reg = view.findViewById(R.id.btn_reg);
        txt_fname = view.findViewById(R.id.txt_usr_fname);
        txt_lname = view.findViewById(R.id.txt_lname);
        txt_email = view.findViewById(R.id.txt_email);
        txt_pwd1 = view.findViewById(R.id.txt_pwd1);
        txt_pwd2 = view.findViewById(R.id.txt_pwd2);

        btn_reg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (txt_fname.getText().toString().length() == 0 || txt_lname.getText().toString().length() == 0 || txt_email.getText().toString().length() == 0) {

                    //Show required error
                    Toast.makeText(getContext(), "All Fields are Required", Toast.LENGTH_SHORT).show();
                } else if (!(txt_pwd1.getText().toString().equals(txt_pwd2.getText().toString()))) {
                    Toast.makeText(getContext(), "Passwords Should Be Matching", Toast.LENGTH_SHORT).show();
                } else {
                    //store the user
                    if (!db.verifyEmail(txt_email.getText().toString().trim()))
                        db.addContact(new User(txt_fname.getText().toString(), txt_lname.getText().toString(), txt_email.getText().toString(), txt_pwd1.getText().toString()));
                    else {
                        Toast.makeText(getContext(), "Email already Exists", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    List<User> user = db.getAllUsers();
                    String result = "";
                    for (User cn : user) {
                        result += "FName: " + cn.getFname() + " - LName: " + cn.getLname() + "- Email:" + cn.getEmail() + "- Pwd:" + cn.getPwd() + " \n";
                        //String log = "Id: " + cn.getID() + " ,Name: " + cn.getName() + " ,Phone: " + cn.getPhoneNumber();
                        // Writing Contacts to log

                    }
                    Log.e("Result: ", result);


                    txt_fname.setText("");
                    txt_lname.setText("");
                    txt_email.setText("");
                    txt_pwd1.setText("");
                    txt_pwd2.setText("");

                    Toast.makeText(getContext(), "New User Registered", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }
}
