package com.navjotelibrary.www.elibary.ui.fragments.admin;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.navjotelibrary.www.elibary.DatabaseHandler;
import com.navjotelibrary.www.elibary.R;
import com.navjotelibrary.www.elibary.ui.models.Item;
import com.navjotelibrary.www.elibary.utils.CurrentScreen;

public class EditBook extends Fragment {

    private DatabaseHandler db;
    EditText txt_itemname, txt_itemprice, txt_isbn, txt_category;
    Button btn_item_edit;
    ImageView cover_book;
    private int itemId;


    public EditBook() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_edit_book, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        db = new DatabaseHandler(getContext());
        txt_itemname = view.findViewById(R.id.txt_itemname);
        txt_itemprice = view.findViewById(R.id.txt_itemprice);
        txt_isbn = view.findViewById(R.id.txt_isbn);
        txt_category = view.findViewById(R.id.txt_category);
        cover_book = view.findViewById(R.id.cover_book);
        btn_item_edit = view.findViewById(R.id.btn_update_item);


        if (getArguments() != null && getContext() != null) {
            String id = getArguments().getString("id");
            itemId = Integer.parseInt(id);

            Item item = db.getItem(itemId);
            txt_itemname.setText(item.getName());
            txt_itemprice.setText(item.getPrice());
            txt_isbn.setText(item.getIsbn());
            txt_category.setText(item.getCategory());

            String drawableName = item.getCover();
            int resID = getResources().getIdentifier(drawableName, "drawable", getContext().getPackageName());
            cover_book.setImageResource(resID);
        }

        btn_item_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (getActivity() != null) {
                    String itemname = txt_itemname.getText().toString();
                    String itemprice = txt_itemprice.getText().toString();
                    String isbn = txt_isbn.getText().toString();
                    String category = txt_category.getText().toString();


                    if (itemname.length() > 0 && itemprice.length() > 0 && isbn.length() > 0 && category.length() > 0) {
                        db.updateItem(itemId, itemname, itemprice, isbn, category);

                        Toast.makeText(getContext(), "Item Details Updated", Toast.LENGTH_LONG).show();
                        getActivity().sendBroadcast(new Intent(CurrentScreen.RECEIVER_BOOK));
                        getActivity().onBackPressed();
                    }
                }
            }
        });


    }
}