package com.navjotelibrary.www.elibary.ui.fragments.common;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.navjotelibrary.www.elibary.DatabaseHandler;
import com.navjotelibrary.www.elibary.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class ForgotPassword extends Fragment implements View.OnClickListener {

    private TextInputEditText etEmail, etNewPassword, etConfirmPassword;
    private TextInputLayout tilEmail, tilNewPassword, tilConfirmPassword;
    private Button btnReset;
    DatabaseHandler db;

    public ForgotPassword() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_forgot_password, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        db = new DatabaseHandler(getContext());

        etEmail = view.findViewById(R.id.et_email);
        etConfirmPassword = view.findViewById(R.id.et_confirm_password);
        etNewPassword = view.findViewById(R.id.et_new_password);

        tilEmail = view.findViewById(R.id.til_email);
        tilConfirmPassword = view.findViewById(R.id.til_confirm_password);
        tilNewPassword = view.findViewById(R.id.til_new_password);

        btnReset = view.findViewById(R.id.button);

        btnReset.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button:
                if (btnReset.getText().equals("Verify Email")) {
                    String email = etEmail.getText().toString().trim();
                    if (!email.isEmpty()) {
                        if (db.verifyEmail(email)) {
                            tilEmail.setVisibility(View.GONE);
                            tilNewPassword.setVisibility(View.VISIBLE);
                            tilConfirmPassword.setVisibility(View.VISIBLE);
                            btnReset.setText("Reset");
                        } else {
                            Toast.makeText(getContext(), "Email doesn't exists.", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getContext(), "Please enter email.", Toast.LENGTH_SHORT).show();
                    }

                } else if (btnReset.getText().equals("Reset")) {
                    String newPassword = etNewPassword.getText().toString().trim();
                    String confirmPassword = etConfirmPassword.getText().toString().trim();

                    if (newPassword.isEmpty() || confirmPassword.isEmpty()) {
                        Toast.makeText(getContext(), "Please enter the fields", Toast.LENGTH_SHORT).show();
                    } else if (!newPassword.equals(confirmPassword)) {
                        Toast.makeText(getContext(), "New password doesn't match with confirm password", Toast.LENGTH_SHORT).show();
                    } else {
                        int updateCount = db.updatePassword(etEmail.getText().toString().trim(), newPassword);
                        System.out.println("ForgotPassword.onClick---->" + updateCount);
                        if (updateCount == 1) {
                            Toast.makeText(getContext(), "Password Updated Successfully.", Toast.LENGTH_SHORT).show();
                            if (getActivity() != null)
                                getActivity().onBackPressed();
                        }
                    }
                }
                break;
        }
    }
}
