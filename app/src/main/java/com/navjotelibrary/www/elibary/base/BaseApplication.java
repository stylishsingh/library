package com.navjotelibrary.www.elibary.base;

import android.app.Application;
import android.content.Context;
import android.content.ContextWrapper;

import com.navjotelibrary.www.elibary.utils.Prefs;


public class BaseApplication extends Application {

    private static Context context;

    public static synchronized Context getGlobalContext() {
        return BaseApplication.context;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        if (BaseApplication.context == null) {
            BaseApplication.context = getApplicationContext();
        }

        new Prefs.Builder()
                .setContext(this)
                .setMode(ContextWrapper.MODE_PRIVATE)
                .setPrefsName(getPackageName())
                .setUseDefaultSharedPreference(true)
                .build();
    }

}