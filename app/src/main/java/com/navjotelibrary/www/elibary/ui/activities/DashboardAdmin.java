package com.navjotelibrary.www.elibary.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.navjotelibrary.www.elibary.R;
import com.navjotelibrary.www.elibary.base.BaseActivity;
import com.navjotelibrary.www.elibary.ui.fragments.admin.AdminBookList;

public class DashboardAdmin extends BaseActivity {

    public Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard_admin);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setTitle("");
        toolbar.setTitle("Dashboard (Admin)");

        navigateToWithBundle(R.id.container, new AdminBookList(), AdminBookList.class.getSimpleName(), false, null);


    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_setting, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_logout:
                startActivity(new Intent(this, LoginActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK));
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
